//
//  User.swift
//  Project
//
//  Created by Mac on 26.10.20.
//

import Foundation

struct User :Codable{
    var id: Int
    var token: String
    var firstname: String
    var lastname: String
    var username: String
    
    init(id:Int, token: String, firstname: String, lastname: String, username: String) {
        self.id = id
        self.token = token
        self.firstname = firstname
        self.lastname = lastname
        self.username = username
    }
}

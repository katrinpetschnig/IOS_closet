//
//  LoginViewController.swift
//  Project
//
//  Created by Mac on 25.10.20.
//

import UIKit
import Foundation

class LoginViewController: UIViewController {
    
    let backgroundImageView = UIImageView()

    @IBOutlet weak var username: DefaultTextField!
    @IBOutlet weak var password: DefaultTextField!
    @IBOutlet weak var login_button: DefaultButton!
    @IBOutlet weak var error_label: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackground()
        // Do any additional setup after loading the view.
    }

    @IBAction func login(_ sender: Any) {
        let un = username.text
        let pd = password.text
        
        if (un == "" || pd == "") {
            return
        }
        
        let params = ["email":un!, "password":pd!] as Dictionary<String, String>

        var request = URLRequest(url: URL(string: "http://localhost:3000/api/login")!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                guard json["token"] != nil else {
                    DispatchQueue.main.async {
                        self.error_label.text = "Username or password incorrect"
                    }
                    return
                }
                
                if json["token"] as! String != "" {
                    DispatchQueue.main.async {
                        let mainView = self.storyboard?.instantiateViewController(withIdentifier: "mainview") as! MainViewController
                        let user = json["user"] as! Dictionary<String, AnyObject>
                        mainView.user = User(id: user["id"] as! Int, token: json["token"] as! String, firstname: user["firstname"] as! String, lastname: user["lastname"] as! String,username: user["email"] as! String)
                        self.navigationController?.pushViewController(mainView, animated: true)
                    }
                }
            } catch {
                print("error")
                DispatchQueue.main.async {
                    self.error_label.text = "Can't login"
                }
            }
        })
        task.resume()
    }
    
    func setBackground() {
        view.addSubview(backgroundImageView)
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundImageView.image = UIImage(named: "wood.JPEG")
        view.sendSubviewToBack(backgroundImageView)
    }
}

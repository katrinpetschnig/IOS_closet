//
//  AddSizeViewController.swift
//  Project
//
//  Created by Mac on 08.11.20.
//

import UIKit

class AddSizeViewController: UIViewController {
    var delegate: AfterPopDelegateSize! = nil

    @IBOutlet weak var size_textfield: UITextField!
    
    @IBOutlet weak var delete_button: UIButton!
    @IBOutlet weak var error_label: UILabel!
    
    var new: Bool = true
    var fromDetail: Bool = true
    var oldValue: String = ""
    var overview: Bool = false
    @IBOutlet weak var name_button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if new == false {
            size_textfield.text = oldValue
            name_button.setTitle("Save", for: .normal)
            delete_button.isHidden = false
        }
    }
    @IBAction func deleteSize(_ sender: UIButton) {
        delegate.deleteSize = true
        delegate.didFinishSize(controller: self)
    }
    @IBAction func buttonClick(_ sender: Any) {
        if size_textfield.text != "" {
            delegate.didFinishSize(controller: self)
        } else {
            error_label.text = "A Size needs a value"
        }
    }
}

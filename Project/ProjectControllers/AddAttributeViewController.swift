//
//  AddAttributeViewController.swift
//  Project
//
//  Created by Mac on 26.10.20.
//

import UIKit

class AddAttributeViewController: UIViewController {
    var delegate: AfterPopDelegateAtt! = nil
    @IBOutlet weak var attribute_name: UITextField!
    @IBOutlet weak var attribute_value: UITextField!
    @IBOutlet weak var error_label: UILabel!
    
    var new: Bool = true
    var fromDetail: Bool = true
    var oldName: String = ""
    var oldValue: String = ""
    var deleteAttr = false
    var overview: Bool = false
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var delete_btn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if new == false {
            attribute_name.text = oldName
            attribute_value.text = oldValue
            btn.setTitle("Save", for: .normal)
            delete_btn.isHidden = false
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func deleteAttr(_ sender: UIButton) {
        deleteAttr = true
        delegate.didFinishA(controller: self)
    }
    @IBAction func buttonClick(_ sender: UIButton) {
        if attribute_name.text != "" && attribute_value.text != "" {
            delegate.didFinishA(controller: self)
        } else {
            error_label.text = "An attribute needs a name and a value";
        }
    }
}

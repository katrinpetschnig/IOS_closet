//
//  Attribute.swift
//  Project
//
//  Created by Mac on 23.10.20.
//

import Foundation

struct Attribute : Codable{
    var key: String
    var value: String
    var id: Int = 0
    var clothid: Int = 0
    
    init(key:String, value: String) {
        self.key = key
        self.value = value
    }
}

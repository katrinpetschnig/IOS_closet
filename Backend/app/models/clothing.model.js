const db = require("./db.js");

// constructor
const Clothing = function(clothing) {
  this.description = clothing.description;
  this.color = clothing.color;
  this.fav = clothing.fav;
  this.size = clothing.size;
  this.image = clothing.image;
  this.categories = clothing.categories;
  this.attributes = clothing.attributes;
};

module.exports = Clothing;
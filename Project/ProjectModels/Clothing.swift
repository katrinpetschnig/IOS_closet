//
//  Clothing.swift
//  Project
//
//  Created by Mac on 23.10.20.
//

import Foundation
import UIKit

struct Clothing : ClothingProtocol, Codable{
    var color: String
    var categories: [Category]
    var description: String?
    var attributes: [Attribute] = []
    var size: Int
    var fav: Bool = false
    var image: String? = nil
    var id: Int = 0
    var userid: Int = 0
    
    //Initializer
    init(size si: Int, color col: String, cat c: String) {
        self.color = col
        self.categories = [];
        self.size = si
    }
}

public class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}

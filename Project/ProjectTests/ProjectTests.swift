//
//  ProjectTests.swift
//  ProjectTests
//
//  Created by Mac on 23.10.20.
//

import XCTest
@testable import Project

class ProjectTests: XCTestCase {

    func testMakeCategory(){
        
        let testCategory = Category(name:"testCategory")
        assert(testCategory.name == "testCategory","Passt net" )
        let testCategory2 = Category(name:"")
        assert(testCategory2.name != "", "Name ist net leer")
        let testCategory3 = Category(name: "testCategory3")
        assert(testCategory3.name == "thisIsNotTheName", "the name of testCategory3 is not thisIsNotTheName")
        let testCategory4 = Category(name:"Dress")
        assert(testCategory4.name.count == 5, "Nope is net 5 ")
    }
    
    func testMakeAttribute(){
        
        let testAttribute = Attribute(key: "Style", value: "Couleurfähig")
        assert(testAttribute.key == "Style" && testAttribute.value == "Couleurfähig", "nope da stimmt was net")
        let testAttribute2 = Attribute(key: "", value: "")
        assert(testAttribute2.key.count > 3, "Key is kürzer als oder gleich 3 Buchstaben")
        let testAttribute3 = Attribute(key: "Style", value: "Sportlich")
        assert(testAttribute.key == testAttribute3.key, "Sind nicht gleich")
    }
    
    func testMakeUser(){
        
        let testUser = User(id: 1, token: "$2a$10$QEKwerDU2AiXXOR/gz91EutGbAXVGFU5cPL63.9ilyGLAos4YjAgW", firstname: "Naruto", lastname: "Uzumaki", username: "Hokage7dattebayo")
        assert(testUser.token.hasPrefix("$"), "Falscher Prefix")
        assert(testUser.token.contains("/") && testUser.token.contains(".") && testUser.token.contains("$"), "Beinhaltet nicht benötigte Zeichen")
        assert(testUser.token.count == 60, "Token hat falsche länge")
        assert(testUser.id == 1, "id stimmt nicht")
    }

    func testMakeClothing(){
        let testAttribute = Attribute(key: "Style", value: "Couleurfähig")
        let testAttribute2 = Attribute(key: "Style", value: "Abendgardarobe")
        
        var testClothing = Clothing(size: 34, color: "black", cat: "Dress")
        testClothing.attributes.append(testAttribute)
        testClothing.attributes.append(testAttribute2)
        testClothing.description?.append("Dieses Kleid ist Knielang und hat kurze Ärmel. Es hat einen leichten Ausschnitt. Des Weiteren besitzt es Taschen.")
        
        assert(testClothing.attributes.count == 2, "Es sollten genau zwei Attribut drin sein.")
        assert(testClothing.size == 34, "Größe müsste 34 sein.")
    }

}

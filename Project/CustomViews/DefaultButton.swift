//
//  DefaultButton.swift
//  Project
//
//  Created by Mac on 23.10.20.
//

import UIKit

class DefaultButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        config()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        config()
    }
    
    private func config() {
        backgroundColor = UIColor(white: 1.0, alpha: 0.2)
        layer.cornerRadius = 25.0
        titleLabel?.font.withSize(30)
        setTitleColor(.white, for: .normal)
    }
}

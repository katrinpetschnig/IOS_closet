//
//  AddColorViewController.swift
//  Project
//
//  Created by Mac on 26.10.20.
//

import UIKit

class AddColorViewController: UIViewController {
    var delegate: AfterPopDelegateCol! = nil
    @IBOutlet weak var colorView: UIView!
    
    var new: Bool = true
    var fromDetail: Bool = true
    var red: CGFloat = 0
    var green: CGFloat = 0
    var blue: CGFloat = 0
    
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        redSlider.value = Float(red)
        greenSlider.value = Float(green)
        blueSlider.value = Float(blue)
        
        updateColor()
        colorView.layer.borderWidth = 5
        colorView.layer.cornerRadius = 20
        colorView.layer.borderColor = UIColor.black.cgColor
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func sliderChanged(_ sender: UISlider) {
        updateColor()
    }
    
    @IBAction func addColor(_ sender: UIButton) {
        delegate.didFinishCol(controller: self)
    }
    
    func updateColor() {
        red = CGFloat(redSlider.value)
        green = CGFloat(greenSlider.value)
        blue = CGFloat(blueSlider.value)
        
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
        colorView.backgroundColor = color
    }
}

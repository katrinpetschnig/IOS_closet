// routes/router.js
const express = require('express');
const router = express.Router();

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const db = require('../models/db.js');
const userMiddleware = require('../middleware/users.js');

//Registrierung eines neuen Users

router.post('/sign-up', userMiddleware.validateRegister, (req, res, next) => {
    db.query(
      `SELECT * FROM User WHERE LOWER(email) = LOWER(${db.escape(
        req.body.email
      )});`,
      (err, result) => {
        if (result.length) {
          return res.status(409).send({
            msg: 'This email is already in use!'
          });
        } else {
          // username is available
          bcrypt.hash(req.body.password, 10, (err, hash) => {
            if (err) {
              return res.status(500).send({
                msg: err
              });
            } else {
              // has hashed pw => add to database
              db.query(
                `INSERT INTO User (email, firstname, lastname, password) VALUES (${db.escape(req.body.email)}, ${db.escape(req.body.firstname)}, ${db.escape(req.body.lastname)}, ${db.escape(hash)})`,
                (err, result) => {
                  if (err) {
                    throw err;
                    return res.status(400).send({
                      msg: err
                    });
                  }
                  return res.status(201).send({
                    msg: 'Registered!'
                  });
                }
              );
            }
          });
        }
      }
    );
  });

//Neuer User logt sich ein

router.post('/login', (req, res, next) => {
    db.query(
      `SELECT * FROM User WHERE email = ${db.escape(req.body.email)};`,
      (err, result) => {
        // user does not exists
        if (err) {
          throw err;
          return res.status(400).send({
            msg: err
          });
        }
  
        if (!result.length) {
          return res.status(401).send({
            msg: 'email or password is incorrect!'
          });
        }
  
        // check password
        bcrypt.compare(
          req.body.password,
          result[0]['password'],
          (bErr, bResult) => {
            // wrong password
            if (bErr) {
              throw bErr;
              return res.status(401).send({
                msg: 'Username or password is incorrect!'
              });
            }
  
            if (bResult) {
              const token = jwt.sign({
                  email: result[0].email,
                  userId: result[0].id
                },
                'IOS_CLOSET', {
                  expiresIn: '7d'
                }
              );
  
              db.query(
                `UPDATE User SET last_login = now() WHERE id = '${result[0].id}'`
              );
              return res.status(200).send({
                msg: 'Logged in!',
                token,
                user: result[0]
              });
            }
            return res.status(401).send({
              msg: 'email or password is incorrect!'
            });
          }
        );
      }
    );
  });

//Alle Kleidungsstücke eines Benutzers returnieren

router.get('/clothes', userMiddleware.isLoggedIn, (req, res, next) => {
    console.log(req.userData);
    let userId = req.userData.userId;
    let counter = 0;
    db.query(`SELECT * FROM Clothing WHERE userid = ${userId}`, (err, result) => {
    if (err) {
      console.log("error: ", err);
      return res.status(400).send({
        msg: err
      });
    }

    let clothRes = result;
    if (clothRes.length == 0) {
      return res.status(400).send({
        msg: "Currently no clothes in Closet"
      });
    }
    clothRes.forEach(cloth => {
        let categories = [];
        db.query(`SELECT * FROM Category WHERE clothid = ${cloth.id}`, (err, result) => {
        if (err) {
          console.log("error: ", err);
          return res.status(400).send({
            msg: err
          });
        }

        Object.keys(result).forEach(function(key) {
            var row = result[key];
            categories.push({id: row.id, name: row.name, clothid: row.clothid});
        });

        let attributes = [];
        db.query(`SELECT * FROM Attribute WHERE clothid = ${cloth.id}`, (err, result) => {
            if (err) {
              console.log("error: ", err);
              return res.status(400).send({
                msg: err
              });
            }
            Object.keys(result).forEach(function(key) {
                var row = result[key];
                attributes.push({id: row.id, key: row.name, value: row.value, clothid: row.clothid});
            });

            cloth.categories = [];
            categories.forEach(cat => {
                cloth.categories.push(cat);
            });
            cloth.attributes = [];
            attributes.forEach(attr => {
                cloth.attributes.push(attr);
            });

            counter++;
            
            if (counter == clothRes.length) {
              let returnClothes = [];
              clothRes.forEach(c => {
                //var base64String = window.btoa(String.fromCharCode.apply(null, new Uint8Array(cloth.image)));
                //cloth.image = cloth.image.toString('base64');
                //cloth.image = "123";
                const newClothing = {
                  description: c.description,
                  color: c.color,
                  fav: c.fav == 1,
                  image: c.image.toString(),
                  size: c.size,
                  categories: c.categories,
                  attributes: c.attributes,
                  userid: userId,
                  id: c.id
                };
                returnClothes.push(JSON.stringify(newClothing));
              });
                
                return res.status(200).send({data: returnClothes});
            }
        });
      });
    });
  });
    
    //res.send('This is the secret content. Only logged in users can see that!');
  });

//Ein bestimmtes Kleidungsstück abfragen

router.get('/clothes/:clothId', userMiddleware.isLoggedIn, (req, res, next) => {
  console.log(req.userData);
  let clothId = req.params.clothId;
  db.query(`SELECT * FROM Clothing WHERE id = ${clothId}`, (err, result) => {
    if (err) {
      console.log("error: ", err);
      return res.status(400).send({
        msg: err
      });
    }



    if (result.length) {
        let cloth = result[0];
      console.log("found clothing: ", result[0]);

      let categories = [];
      db.query(`SELECT * FROM Category WHERE clothid = ${clothId}`, (err, result) => {
        if (err) {
          console.log("error: ", err);
          return res.status(400).send({
            msg: err
          });
        }
    
        if (result.length) {
          console.log("found categories: ", result);
        }

        Object.keys(result).forEach(function(key) {
            var row = result[key];
            categories.push({id: row.id, name: row.name, clothid: row.clothid});
        });

        let attributes = [];
        db.query(`SELECT * FROM Attribute WHERE clothid = ${clothId}`, (err, result) => {
            if (err) {
              console.log("error: ", err);
              return res.status(400).send({
                msg: err
              });
            }
        
            if (result.length) {
            console.log("found attributes: ", result);
            }

            Object.keys(result).forEach(function(key) {
                var row = result[key];
                attributes.push({id: row.id, key: row.key, value: row.value, clothid: row.clothid});
            });

            cloth.categories = [];
            categories.forEach(cat => {
                cloth.categories.push(cat);
            });
            cloth.attributes = [];
            attributes.forEach(attr => {
                cloth.attributes.push(attr);
            });

            return res.status(200).send({data: cloth});
        });
      });
    }
  });
});

//Neues Kleidungsstück anlegen

router.post('/clothes', userMiddleware.isLoggedIn, (req, res, next) => {
  let userId = req.userData.userId;
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  const newClothing = {
    description: req.body.description,
    color: req.body.color,
    fav: req.body.fav,
    image: req.body.image,
    size: req.body.size,
    categories: req.body.categories,
    attributes: req.body.attributes,
    userid: userId
  };


  db.query("INSERT INTO Clothing (userid, description, color, size, image, fav) values(?,?,?,?,?,?)", 
  [userId,  newClothing.description, newClothing.color, newClothing.size, newClothing.image, newClothing.fav], (err, result) => {
    if (err) {
      console.log("error: ", err);
      return res.status(400).send({
        msg: err
      });
    }

    let clothId = result.insertId;
    let count = 1;
    newClothing.id = clothId;
    if (newClothing.categories.length == 0) {
      let countAttr = 1;
      newClothing.attributes.forEach(attr => {
        db.query("INSERT INTO Attribute (clothid, name, value) values(?,?,?)", 
        [clothId, attr.key, attr.value], (err, result) => {
          if (err) {
            console.log("error: ", err);
            return res.status(400).send({
              msg: err
            });
          }
          attr.id = result.insertId;
          attr.clothId;
          if (countAttr == newClothing.attributes.length) {
            return res.status(200).send({data: JSON.stringify(newClothing), id: clothId});
          }
          countAttr++;
        });
      });
      if (newClothing.attributes.length == 0) {
        return res.status(200).send({data: JSON.stringify(newClothing), id: clothId});
      }
    }
    newClothing.categories.forEach(cat => {
        db.query("INSERT INTO Category (name, clothid) values(?,?)", 
        [cat.name, clothId], (err, result) => {
          if (err) {
            console.log("error: ", err);
            return res.status(400).send({
              msg: err
            });
          }

          cat.id = result.insertId;
          cat.clothid = clothId;
          if (count == newClothing.categories.length) {
            let countAttr = 1;
              newClothing.attributes.forEach(attr => {
                db.query("INSERT INTO Attribute (clothid, name, value) values(?,?,?)", 
                [clothId, attr.key, attr.value], (err, result) => {
                  if (err) {
                    console.log("error: ", err);
                    return res.status(400).send({
                      msg: err
                    });
                  }
                  attr.id = result.insertId;
                  attr.clothid = clothId;
                  if (countAttr == newClothing.attributes.length) {
                    console.log("ERSTELLT:");
                    console.log(newClothing.image.substring(0,20))
                    return res.status(200).send({data: JSON.stringify(newClothing), id: clothId});
                  }
                  countAttr++;
                });
              });
            if (newClothing.attributes.length == 0) {
              return res.status(200).send({data: JSON.stringify(newClothing), id: clothId});
            }
          }
          count++;
        });
    });
  });
});

//Kleidungsstück updaten

router.post('/clothes/:clothId', userMiddleware.isLoggedIn, (req, res, next) => {
  console.log(req.userData);
  let id = req.params.clothId;

  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }
  const clothing = new Clothing(req.body);
  db.query(
    "UPDATE Clothing SET description = ?, fav = ?, image = ?, color = ?, size = ? WHERE id = ?",
    [lothing.description, clothing.fav,clothing.image, clothing.color,clothing.size, id],
    (err, result) => {
      if (err) {
        console.log("error: ", err);
        return res.status(400).send({
          msg: err
        });
      }

      clothing.categories.forEach(cat => {
        db.query(
            "UPDATE Category SET name = ? WHERE id = ?",
            [cat.name, cat.id],
            (err, result) => {
              if (err) {
                console.log("error: ", err);
                return res.status(400).send({
                  msg: err
                });
              }
        
              console.log("updated categories: ", { id: id, ...cat });
              clothing.attributes.forEach(attr => {
                db.query(
                    "UPDATE Attribute SET key = ?, value = ? WHERE id = ?",
                    [attr.key, attr.value, attr.id],
                    (err, result) => {
                      if (err) {
                        console.log("error: ", err);
                        return res.status(400).send({
                          msg: err
                        });
                      }
                
                      console.log("updated attributes: ", { id: id, ...attr });
                      console.log("updated clothing: ", { id: id, ...clothing });
                      return res.status(200).send({data: clothing, id: id});
                    }
                  );
              });
            }
          );
      });
    }
  );
});

//Löschen eines Kleidungsstückes

router.delete('/clothes/:clothId', userMiddleware.isLoggedIn, (req, res, next) => {
  console.log(req.userData);
  let id = req.params.clothId;
  db.query("DELETE FROM Attribute WHERE clothid = ?", id, (err, result) => {
    if (err) {
        console.log("error: ", err);
        return res.status(400).send({
          msg: err
        });
    }

    console.log("deleted attributes with clothid: ", id);
    
    db.query("DELETE FROM Category WHERE clothid = ?", id, (err, result) => {
        if (err) {
            console.log("error: ", err);
            return res.status(400).send({
              msg: err
            });
        }

        console.log("deleted categories with clothid: ", id);
        db.query("DELETE FROM Clothing WHERE id = ?", id, (err, result) => {
            if (err) {
              console.log("error: ", err);
              return res.status(400).send({
                msg: err
              });
            }
        
            if (result.affectedRows == 0) {
              // not found Customer with the id
              return res.status(400).send({
                msg: "Not Found"
              });
            }
        
            console.log("deleted clothing with id: ", id);
            return res.status(200).send({deleted: true, id: id});
          });
    });
});
});

//Neue Kategorie anlegen

router.post('/category', userMiddleware.isLoggedIn, (req, res, next) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  db.query("INSERT INTO Category (name, clothid) values(?,?)", 
    [req.body.name, req.body.clothId], (err, result) => {
      if (err) {
        console.log("error: ", err);
        return res.status(400).send({
          msg: err
        });
      }
  
      console.log("created category: ", { id: result.insertId });
      return res.status(200).send({data: {id: result.insertId, name: req.body.name, clothId: req.body.clothId}});
    });
});

//Neues Attribut anlegen

router.post('/attribute', userMiddleware.isLoggedIn, (req, res, next) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

    db.query("INSERT INTO Attribute (clothid, key, value) values(?,?,?)", 
    [req.body.clothId, req.body.key, req.body.value], (err, result) => {
      if (err) {
        console.log("error: ", err);
        return res.status(400).send({
          msg: err
        });
      }
  
      console.log("created attribute: ", { id: result.insertId });
      return res.status(200).send({data: {id: result.insertId, 
        key: req.body.key, value: req.body.value, clothId: req.body.clothId}});
    
    });
});

module.exports = router;
//
//  AddCategoryViewController.swift
//  Project
//
//  Created by Mac on 26.10.20.
//

import UIKit

class AddCategoryViewController: UIViewController {

    @IBOutlet weak var name_label: UILabel!
    @IBOutlet weak var name_button: UIButton!
    @IBOutlet weak var name_textfield: UITextField!
    @IBOutlet weak var delete_btn: UIButton!
    @IBOutlet weak var error_label: UILabel!
    
    var delegate: AfterPopDelegateCat! = nil
    
    var new: Bool = true
    var fromDetail: Bool = true
    var oldValue: String = ""
    var overview: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if new == false {
            name_textfield.text = oldValue
            name_button.setTitle("Save", for: .normal)
            delete_btn.isHidden = false
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func deleteCategory(_ sender: UIButton) {
        delegate.deleteCat = true
        delegate.didFinishC(controller: self)
    }
    @IBAction func buttonClick(_ sender: Any) {
        if name_textfield.text != "" {
            delegate.didFinishC(controller: self)
        } else {
            error_label.text = "A Category needs a name"
        }
    }
}

//
//  Category.swift
//  Project
//
//  Created by Mac on 27.10.20.
//

import Foundation

struct Category : Codable{
    var id: Int
    var name: String
    var clothid: Int = 0
    
    init(name: String) {
        self.id = 0
        self.name = name
    }
}

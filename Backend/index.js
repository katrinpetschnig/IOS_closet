const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const cors = require("cors");

// set up port
const PORT = 3000;

// parse requests of content-type: application/json
app.use(bodyParser.json({
  limit: '50mb'
}));
app.use(cors());
//app.use(express.bodyParser({limit: '50mb'}));

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ 
  limit: '50mb',
  parameterLimit: 100000,
  extended: true 
}));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});

const router = require('./app/routes/router.js');
app.use('/api', router);

// set port, listen for requests
app.listen(PORT, () => {
  console.log("Server is running on port 3000.");
});
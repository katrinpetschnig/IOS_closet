//
//  RegisterViewController.swift
//  Project
//
//  Created by Mac on 25.10.20.
//

import UIKit

class RegisterViewController: UIViewController {

    let backgroundImageView = UIImageView()
    @IBOutlet weak var email_textfield: DefaultTextField!
    @IBOutlet weak var firstname_textfield: DefaultTextField!
    @IBOutlet weak var lastname_textfield: DefaultTextField!
    @IBOutlet weak var password_textfield: DefaultTextField!
    @IBOutlet weak var password_repeat_textfield: DefaultTextField!
    @IBOutlet weak var error_label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackground()
    }
    
    func setBackground() {
        view.addSubview(backgroundImageView)
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundImageView.image = UIImage(named: "wood.JPEG")
        view.sendSubviewToBack(backgroundImageView)
    }
    
    @IBAction func register_new_user(_ sender: Any) {
        let email = email_textfield.text
        let firstname = firstname_textfield.text
        let lastname = lastname_textfield.text
        
        let password = password_textfield.text
        let password_repeat = password_repeat_textfield.text
        
        if (email == "") {
            error_label.text = "Email needs to be entered"
            return
        }
        
        if (firstname == "") {
            error_label.text = "Firstname needs to be entered"
            return
        }
        
        if (lastname == "") {
            error_label.text = "Lastname needs to be entered"
            return
        }
        
        if (password == "") {
            error_label.text = "password needs to be entered"
            return
        }
        
        if (password != password_repeat) {
            error_label.text = "Password repeat doesn't match password"
            return
        }
        
        error_label.text = ""
        
        let params = ["email":email!, "firstname":firstname!,"lastname":lastname!, "password":password!, "password_repeat":password_repeat!] as Dictionary<String, String>

        var request = URLRequest(url: URL(string: "http://localhost:3000/api/sign-up")!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                if json["msg"] as! String == "Registered!" {
                    DispatchQueue.main.async {
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.error_label.text = "User couldn't be created"
                    }
                }
            } catch {
                print("error")
            }
        })
        task.resume()
    }
}

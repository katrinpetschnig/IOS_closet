//
//  AfterPupDelegate.swift
//  Project
//
//  Created by Mac on 27.10.20.
//

import Foundation

protocol AfterPopDelegateCat {
    func didFinishC(controller: AddCategoryViewController)
    var deleteCat:Bool {get set}
}

protocol AfterPopDelegateSize {
    func didFinishSize(controller: AddSizeViewController)
    var deleteSize:Bool {get set}
}

protocol AfterPopDelegateAtt {
    func didFinishA(controller: AddAttributeViewController)
}

protocol AfterPopDelegateCol {
    func didFinishCol(controller: AddColorViewController)
}

protocol AfterDetailDelegate {
    func didFinishCreate(controller: DetailViewController)
}

//
//  Closet.swift
//  Project
//
//  Created by Mac on 23.10.20.
//

import Foundation
import UIKit

class Closet {
    var name : String = ""
    var pieces: [Clothing] = []
    {
        //OBSERVABLES
        willSet(newValue){
            print(newValue.count, terminator: "")
            print(" Kleidungsstück(e) im Kleiderschrank")
        }
    }

    /*____________________FUNKTIONEN_____________________*/
    
    //Gesamten Kleiderschrank ausgeben
    func printCloset() {
        print("GANZEN KLEIDERSCHARNK (" + name + ") AUSGEBEN:")
        for c in pieces {
            print(c.categories, terminator: "")
            print(" ->  [")
            print(c.color, terminator: "")
            print("] in Größe " + String(c.size))
        }
    }
    
    //Gibt alle Kleidungsstücke mit dieser Farbe aus
    func printClothesWithColor(color: String) {
        let filtered = pieces.filter {$0.color == color}
        print(" NACH FARBE ", terminator: "")
        print(color, terminator: "")
        print(" FILTERN:")
        for c in filtered {
            print(c.categories, terminator: "")
            print(" -> " + " [", terminator: "")
            print(c.color, terminator: "")
            print("] in Größe " + String(c.size))
        }
        print()
    }
    
    //Löscht entweder alle mit diesem Namen oder alle Kleidungsstücke heraus
    func removeClothes(id: Int?) {
        if let i = id {
            pieces = pieces.filter {$0.id != i}
        } else {
            pieces = []
        }
    }
    
    //OPERATOR OVERLOADING
    static func + (left: Closet, right: Closet) -> Closet {
        let new = Closet()
        
        //Alle Kleidungsstücke von Links
        for piece in left.pieces {
            new.pieces.append(piece)
        }
        //Alle Kleidungsstücke von Rechts
        for piece in right.pieces {
            new.pieces.append(piece)
        }
        
        return new
    }
}

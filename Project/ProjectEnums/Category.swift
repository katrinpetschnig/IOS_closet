//
//  Category.swift
//  Project
//
//  Created by Mac on 23.10.20.
//

import Foundation

enum Category {
    case shirt
    case pant
    case dress
    case skirt
    case shoes
    case jacket
    case shorts
}

use backend;

drop table if exists `Category` ;
drop table if exists `Attribute` ;
drop table if exists `Clothing` ;
drop table if exists `User` ;

CREATE TABLE `User` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`firstname` VARCHAR(255) NOT NULL,
	`lastname` VARCHAR(255) NOT NULL,
	`email` VARCHAR(255) NOT NULL,
	`password` VARCHAR(255) NOT NULL,
	`last_login` datetime,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Clothing` (
	`id` INT NOT NULL AUTO_INCREMENT,
    `userid` INT NOT NULL,
	`description` VARCHAR(255),
	`size` INT NOT NULL,
	`color` VARCHAR(255) NOT NULL,
	`image` LONGBLOB NOT NULL,
    `fav` bool NOT NULL default false,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Attribute` (
	`clothid` INT NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`value` VARCHAR(255) NOT NULL,
	`id` INT NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Category` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`clothid` INT NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `Clothing` ADD CONSTRAINT `Clothing_fk0` FOREIGN KEY (`userid`) REFERENCES `User`(`id`);

ALTER TABLE `Attribute` ADD CONSTRAINT `Attribute_fk0` FOREIGN KEY (`clothid`) REFERENCES `Clothing`(`id`);

ALTER TABLE `Category` ADD CONSTRAINT `Category_fk0` FOREIGN KEY (`clothid`) REFERENCES `Clothing`(`id`);

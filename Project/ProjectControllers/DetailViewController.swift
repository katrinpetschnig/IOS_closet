//
//  DetailViewController.swift
//  Project
//
//  Created by Mac on 23.10.20.
//

import UIKit

class DetailViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate , UIPopoverPresentationControllerDelegate, AfterPopDelegateCat, AfterPopDelegateAtt, AfterPopDelegateCol, UITextFieldDelegate{

    let backgroundImageView = UIImageView()
    var user: User? = nil
    var item: Clothing? = nil
    @IBOutlet weak var error_label: UILabel!
    @IBOutlet weak var categories_list: UIScrollView!
    @IBOutlet weak var btn: DefaultButton!
    @IBOutlet weak var size_textfield: UITextField!
    @IBOutlet var import_btn: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var category_stackview: UIStackView!
    @IBOutlet weak var attribute_stackview: UIStackView!
    @IBOutlet weak var fav_image: UIImageView!
    @IBOutlet weak var fav_btn: UIButton!
    @IBOutlet weak var description_field: UITextField!
    var delegate: AfterDetailDelegate! = nil
    @IBOutlet weak var color_show: UIButton!
    
    var deleteCat: Bool = false
    
    @IBAction func changeFav(_ sender: UIButton) {
        if item?.fav == true {
            item?.fav = false;
            fav_image.image = UIImage(named: "star_gray.png")
        } else {
            item?.fav = true;
            fav_image.image = UIImage(named: "star.png")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Clothing"
        item = Clothing(size: 0,color: "", cat: "")
        setBackground()
        color_show.backgroundColor = .white
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let _ = string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) {
            return true
        } else {
            return false
        }
    }
    
    func fillClothingItem () {
        item?.description = description_field.text
        let color = color_show.backgroundColor
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0

        color?.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        item?.color = "\(red),\(green),\(blue)"
        let str = size_textfield?.text ?? ""
        item?.size = Int(str)  ?? 0
        item?.categories = []
        for cat in category_stackview.arrangedSubviews {
            let btn = cat as! UIButton
            if let txt = btn.titleLabel?.text {
                item?.categories.append(Category(name: txt))
            }
        }
        
        item?.attributes = []
        for attr in attribute_stackview.arrangedSubviews {
            let btn = attr as! UIButton
            let str = btn.titleLabel?.text ?? ""
            let arr = str.components(separatedBy: " - ")
            let key = arr[0]
            let value = arr[1]
            
            item?.attributes.append(Attribute(key: key, value: value))
        }
        let jpeg = imageView.image?.jpegData(compressionQuality: 0.50) as! Data
        item?.image = jpeg.base64EncodedString()
    }
    
    @IBAction func createNewClothing(_ sender: UIButton) {
        guard imageView.image != nil else {
            error_label.text = "A clothing item needs an image"
            return
        }
        
        guard size_textfield.text != "" else {
            error_label.text = "A clothing item needs as size"
            return
        }
        fillClothingItem()
        let jsonEncoder = JSONEncoder()
        
        do {
            let jsonData = try jsonEncoder.encode(item)
            //let jsonstr = String(data: jsonData, encoding: String.Encoding.utf16)
        
            var request = URLRequest(url: URL(string: "http://localhost:3000/api/clothes")!)
            request.httpMethod = "POST"
            request.httpBody = jsonData
            let token = user?.token ?? ""
            request.addValue("Bearer " + token, forHTTPHeaderField: "authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")

                
            let session = URLSession.shared
            let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
                do {
                    let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                    guard json["id"] != nil else {
                        DispatchQueue.main.async {
                            self.error_label.text = "Could not create Clothing item"
                        }
                        return
                    }
                    
                    DispatchQueue.main.async {
                        
                        let jsonString = json["data"] as! String
                        print("json " + jsonString)
                        if let jsonD = jsonString.data(using: .utf8)
                        {
                            let decoder = JSONDecoder()

                            do {
                                let cloth = try decoder.decode(Clothing.self, from: jsonD)
                                print(cloth)
                                self.item = cloth
                            } catch {
                                print(error.localizedDescription)
                            }
                        }
                        self.delegate.didFinishCreate(controller: self)
                    }
                    
                } catch {
                    print("error")
                    DispatchQueue.main.async {
                        self.error_label.text = "Can't login"
                    }
                }
            })
            task.resume()
        } catch {
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            imageView.image = image
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func importImage(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        image.allowsEditing = false
        image.sourceType = .photoLibrary
        //image.sourceType =  UIImagePickerControllerSourceType.photoLibrary
        self.present(image, animated: true){}
    }
    
    func setBackground() {
        view.addSubview(backgroundImageView)
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundImageView.image = UIImage(named: "wood.JPEG")
        view.sendSubviewToBack(backgroundImageView)
    }
    @IBAction func addCategory(_ sender: Any) {
        let button = sender as? UIButton
        let buttonFrame = button?.frame ?? CGRect.zero
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "categoryView") as! AddCategoryViewController
        
        popoverContentController.modalPresentationStyle = .popover
        popoverContentController.delegate = self
         
        if let popoverPresentationController = popoverContentController.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = buttonFrame
            popoverPresentationController.delegate = self
            present(popoverContentController, animated: true, completion: nil)
        }
    }
    @IBAction func addColor(_ sender: Any) {
        let color = color_show.backgroundColor
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0

        color?.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        let button = sender as? UIButton
        let buttonFrame = button?.frame ?? CGRect.zero
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "colorView") as! AddColorViewController
        
        popoverContentController.modalPresentationStyle = .popover
        popoverContentController.delegate = self
        popoverContentController.red = red;
        popoverContentController.green = green;
        popoverContentController.blue = blue;
         
        if let popoverPresentationController = popoverContentController.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = buttonFrame
            popoverPresentationController.delegate = self
            present(popoverContentController, animated: true, completion: nil)
        }
    }
    @IBAction func addAttribute(_ sender: Any) {
        let button = sender as? UIButton
        let buttonFrame = button?.frame ?? CGRect.zero
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "attributeView") as! AddAttributeViewController
        
        popoverContentController.modalPresentationStyle = .popover
        popoverContentController.delegate = self
         
        if let popoverPresentationController = popoverContentController.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = buttonFrame
            popoverPresentationController.delegate = self
            present(popoverContentController, animated: true, completion: nil)
        }
    }
    
    func didFinishC(controller: AddCategoryViewController) {
        if controller.new {
            if let txt = controller.name_textfield.text {
                let newCat = Category(name:txt)
                item?.categories.append(newCat)
                
                DispatchQueue.main.async {
                    let catBtn: UIButton = UIButton()
                    catBtn.setTitle(newCat.name, for: .normal)
                    catBtn.backgroundColor = .black
                    catBtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
                    catBtn.layer.borderWidth = 2.0
                    catBtn.translatesAutoresizingMaskIntoConstraints = false
                    catBtn.widthAnchor.constraint(equalToConstant: 150).isActive = true
                    catBtn.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
                    catBtn.layer.borderColor = .init(red: 1.0, green: 1.0, blue: 1.0, alpha: 1);
                    catBtn.addTarget(self, action: #selector(self.editCategoryClick), for: .touchUpInside)
                    self.category_stackview.addArrangedSubview(catBtn)
                }
            }
        } else {
            for cat in category_stackview.arrangedSubviews {
                let btn = cat as! UIButton
                if btn.titleLabel?.text == controller.oldValue {
                    if deleteCat == true {
                        category_stackview.removeArrangedSubview(cat)
                        cat.removeFromSuperview()
                    } else {
                        btn.setTitle(controller.name_textfield.text, for: .normal)
                    }
                }
            }
            for var cat in item?.categories ?? []{
                if cat.name == controller.oldValue {
                    cat.name = controller.name_textfield.text ?? ""
                
                }
            }
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    @objc func editCategoryClick(_ sender: UIButton) {
        let button = sender
        let buttonFrame = button.frame
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "categoryView") as! AddCategoryViewController
        
        popoverContentController.modalPresentationStyle = .popover
        popoverContentController.delegate = self
        popoverContentController.new = false
        let name = button.titleLabel?.text ?? ""
        
        popoverContentController.oldValue = name
         
        if let popoverPresentationController = popoverContentController.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = buttonFrame
            popoverPresentationController.delegate = self
            present(popoverContentController, animated: true, completion: nil)
        }
    }
    
    func didFinishA(controller: AddAttributeViewController) {
        if controller.new {
            if let attr_name = controller.attribute_name.text {
                if let attr_value = controller.attribute_value.text {
                    let newAttr = Attribute(key: attr_name, value: attr_value)
                    item?.attributes.append(newAttr)
                    
                    DispatchQueue.main.async {
                        let attrBtn: UIButton = UIButton()
                        attrBtn.setTitle(newAttr.key + " - " + newAttr.value, for: .normal)
                        attrBtn.translatesAutoresizingMaskIntoConstraints = false
                        attrBtn.widthAnchor.constraint(equalToConstant: 200).isActive = true
                        attrBtn.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
                        attrBtn.backgroundColor = .black
                        attrBtn.addTarget(self, action: #selector(self.editAttributeClick), for: .touchUpInside)
                        attrBtn.layer.borderWidth = 2.0
                        attrBtn.layer.borderColor = .init(red: 1.0, green: 1.0, blue: 1.0, alpha: 1);
                        self.attribute_stackview.addArrangedSubview(attrBtn)
                    }
                }
            }
        } else {
            for attr in attribute_stackview.arrangedSubviews {
                let btn = attr as! UIButton
                let str = btn.titleLabel?.text ?? ""
                let arr = str.components(separatedBy: " - ")
                let key = arr[0]
                let value = arr[1]
                
                if key == controller.oldName && value == controller.oldValue {
                    if controller.deleteAttr == true {
                        attribute_stackview.removeArrangedSubview(attr)
                        attr.removeFromSuperview()
                    } else {
                        let keyNew = controller.attribute_name.text ?? ""
                        let valueNew = controller.attribute_value.text ?? ""
                        let newTxt = keyNew + " - " + valueNew
                        btn.setTitle(newTxt, for: .normal)
                    }
                }
            }
            if controller.deleteAttr == true {
                let filtered = item?.attributes ?? []
                item?.attributes = filtered.filter{$0.key == controller.oldName && $0.value == controller.oldValue}
            } else {
                for var attr in item?.attributes ?? []{
                    if attr.key == controller.oldName && attr.value == controller.oldValue {
                        let keyNew = controller.attribute_name.text ?? ""
                        let valueNew = controller.attribute_value.text  ?? ""
                        attr.key = keyNew
                        attr.value = valueNew
                    }
                }
            }
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    @objc func editAttributeClick(_ sender: UIButton) {
        let button = sender
        let buttonFrame = button.frame
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "attributeView") as! AddAttributeViewController
        
        popoverContentController.modalPresentationStyle = .popover
        popoverContentController.delegate = self
         
        if let popoverPresentationController = popoverContentController.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = buttonFrame
            popoverPresentationController.delegate = self
            popoverContentController.new = false
            let str = button.titleLabel?.text ?? ""
            let arr = str.components(separatedBy: " - ")
            let key = arr[0]
            let value = arr[1]
            
            popoverContentController.oldName = key
            popoverContentController.oldValue = value
            present(popoverContentController, animated: true, completion: nil)
        }
    }
    
    func didFinishCol(controller: AddColorViewController) {
        if controller.new {
            color_show.backgroundColor = controller.colorView.backgroundColor
        }
        controller.dismiss(animated: true, completion: nil)
    }
}

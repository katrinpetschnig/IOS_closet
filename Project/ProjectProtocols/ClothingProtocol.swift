//
//  ClothingProtocol.swift
//  Project
//
//  Created by Mac on 23.10.20.
//

import Foundation
import UIKit

protocol ClothingProtocol {
    var color: String {get set}
    var categories: [Category] {get set}
    var description: String? {get set}
    var attributes: [Attribute] {get set}
    var size: Int {get set}
    var image: String? {get set}
    var id: Int {get set}
}

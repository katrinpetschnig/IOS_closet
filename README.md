# IOS Closet

IDEA: virtual closet on your phone
TEAM: Verena Leifert, Katrin Petschnig

DESCRIPTION: 
- A listing of pictures of clothes that can be filtered, and modified
- New Clothes can be added with different attributes and values that later   
  can be filtered
- A Attribute contains a key and a value that are linked together
- Clothes can also be marked as favorite
- A user can register and login to the virtual closet
- The closet has 5 Models (User, Closet, Clothing, Category and Attribute)
- Authentication happens over a JWT

FEATURES: 
- Web Services
- Unit Tests
- System Services
- Multi Language Support (English and German)

HOW TO MAKE IT RUN?
- Make sure nodejs is installed
- Make sure a MySQL Database with the parameters in db.config.js is provided 
  with a database called "backend"
- Start the create.sql script in your db
- If you want some testdata you can also use the insert.sql script
- Go into the backend folder and run "npm install"
- After that run "node index.js" and you should se those 2 lines:
  Server is running on port 3000
  Successfully connected to the database
- Now you can start the simulator via XCode and can register or login to your 
  closet


//
//  MainViewController.swift
//  Project
//
//  Created by Mac on 23.10.20.
//

import UIKit
import Dispatch

class MainViewController: UIViewController, AfterDetailDelegate, UICollectionViewDelegate, UICollectionViewDataSource, AfterPopDelegateCat, UIPopoverPresentationControllerDelegate,AfterPopDelegateAtt, AfterPopDelegateSize, AfterPopDelegateCol{
    func didFinishSize(controller: AddSizeViewController) {
        if controller.new {
            if let txt = controller.size_textfield.text {
                sizes.append(Int(txt) ?? 0)
                
                DispatchQueue.main.async {
                    let catBtn: UIButton = UIButton()
                    catBtn.setTitle(txt, for: .normal)
                    catBtn.backgroundColor = .black
                    catBtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
                    catBtn.layer.borderWidth = 2.0
                    catBtn.translatesAutoresizingMaskIntoConstraints = false
                    catBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
                    catBtn.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
                    catBtn.layer.borderColor = .init(red: 1.0, green: 1.0, blue: 1.0, alpha: 1);
                    catBtn.addTarget(self, action: #selector(self.editSizeClick), for: .touchUpInside)
                    self.size_stackview.addArrangedSubview(catBtn)
                }
            }
        } else {
            for size in size_stackview.arrangedSubviews {
                let btn = size as! UIButton
                if btn.titleLabel?.text == controller.oldValue {
                    if deleteSize == true {
                        size_stackview.removeArrangedSubview(size)
                        size.removeFromSuperview()
                    } else {
                        btn.setTitle(controller.size_textfield.text, for: .normal)
                    }
                }
            }
            for var size in sizes {
                if size == Int(controller.oldValue) ?? 0 {
                    let str = controller.size_textfield.text ?? "0"
                    size = Int(str) ?? 0
                }
            }
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    @objc func editSizeClick(_ sender: UIButton) {
        let button = sender
        let buttonFrame = button.frame
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "sizeView") as! AddSizeViewController
        
        popoverContentController.modalPresentationStyle = .popover
        popoverContentController.delegate = self
        popoverContentController.new = false
        let name = button.titleLabel?.text ?? ""
        
        popoverContentController.oldValue = name
         
        if let popoverPresentationController = popoverContentController.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = buttonFrame
            popoverPresentationController.delegate = self
            present(popoverContentController, animated: true, completion: nil)
        }
    }
    
    var deleteSize: Bool = false
    
    func didFinishCol(controller: AddColorViewController) {
        
    }
    
    
    var deleteCat: Bool = false
    
    @IBOutlet weak var categories_stackview: UIStackView!
    @IBOutlet weak var attribute_stackview: UIStackView!
    @IBOutlet weak var size_stackview: UIStackView!
    @IBOutlet weak var color_stackview: UIStackView!
    
    var categories: [Category] = []
    var attributes: [Attribute] = []
    var sizes: [Int] = []
    var colors: [UIColor] = []
    
    @IBAction func addSize(_ sender: UIButton) {
        let buttonFrame = sender.frame
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "sizeView") as! AddSizeViewController
        
        popoverContentController.modalPresentationStyle = .popover
        popoverContentController.delegate = self
        popoverContentController.overview = true
         
        if let popoverPresentationController = popoverContentController.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = buttonFrame
            popoverPresentationController.delegate = self
            present(popoverContentController, animated: true, completion: nil)
        }
    }
    func didFinishA(controller: AddAttributeViewController) {
        if controller.new {
            if let attr_name = controller.attribute_name.text {
                if let attr_value = controller.attribute_value.text {
                    let newAttr = Attribute(key: attr_name, value: attr_value)
                    attributes.append(newAttr)
                    
                    DispatchQueue.main.async {
                        let attrBtn: UIButton = UIButton()
                        attrBtn.setTitle(newAttr.key + " - " + newAttr.value, for: .normal)
                        attrBtn.translatesAutoresizingMaskIntoConstraints = false
                        attrBtn.widthAnchor.constraint(equalToConstant: 200).isActive = true
                        attrBtn.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
                        attrBtn.backgroundColor = .black
                        attrBtn.addTarget(self, action: #selector(self.editAttributeClick), for: .touchUpInside)
                        attrBtn.layer.borderWidth = 2.0
                        attrBtn.layer.borderColor = .init(red: 1.0, green: 1.0, blue: 1.0, alpha: 1);
                        self.attribute_stackview.addArrangedSubview(attrBtn)
                    }
                }
            }
        } else {
            for attr in attribute_stackview.arrangedSubviews {
                let btn = attr as! UIButton
                let str = btn.titleLabel?.text ?? ""
                let arr = str.components(separatedBy: " - ")
                let key = arr[0]
                let value = arr[1]
                
                if key == controller.oldName && value == controller.oldValue {
                    if controller.deleteAttr == true {
                        attribute_stackview.removeArrangedSubview(attr)
                        attr.removeFromSuperview()
                    } else {
                        let keyNew = controller.attribute_name.text ?? ""
                        let valueNew = controller.attribute_value.text ?? ""
                        let newTxt = keyNew + " - " + valueNew
                        btn.setTitle(newTxt, for: .normal)
                    }
                }
            }
            if controller.deleteAttr == true {
                attributes = attributes.filter{$0.key == controller.oldName && $0.value == controller.oldValue}
            } else {
                for var attr in attributes {
                    if attr.key == controller.oldName && attr.value == controller.oldValue {
                        let keyNew = controller.attribute_name.text ?? ""
                        let valueNew = controller.attribute_value.text  ?? ""
                        attr.key = keyNew
                        attr.value = valueNew
                    }
                }
            }
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    @objc func editAttributeClick(_ sender: UIButton) {
        let button = sender
        let buttonFrame = button.frame
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "attributeView") as! AddAttributeViewController
        
        popoverContentController.modalPresentationStyle = .popover
        popoverContentController.delegate = self
         
        if let popoverPresentationController = popoverContentController.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = buttonFrame
            popoverPresentationController.delegate = self
            popoverContentController.new = false
            let str = button.titleLabel?.text ?? ""
            let arr = str.components(separatedBy: " - ")
            let key = arr[0]
            let value = arr[1]
            
            popoverContentController.oldName = key
            popoverContentController.oldValue = value
            present(popoverContentController, animated: true, completion: nil)
        }
    }
    
    @IBAction func addAttribute(_ sender: UIButton) {
        let buttonFrame = sender.frame
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "attributeView") as! AddAttributeViewController
        
        popoverContentController.modalPresentationStyle = .popover
        popoverContentController.delegate = self
        popoverContentController.overview = true
         
        if let popoverPresentationController = popoverContentController.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = buttonFrame
            popoverPresentationController.delegate = self
            present(popoverContentController, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return closet.pieces.count
    }
    
    func didFinishC(controller: AddCategoryViewController) {
        if controller.new {
            if let txt = controller.name_textfield.text {
                let newCat = Category(name:txt)
                categories.append(newCat)
                
                DispatchQueue.main.async {
                    let catBtn: UIButton = UIButton()
                    catBtn.setTitle(newCat.name, for: .normal)
                    catBtn.backgroundColor = .black
                    catBtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
                    catBtn.layer.borderWidth = 2.0
                    catBtn.translatesAutoresizingMaskIntoConstraints = false
                    catBtn.widthAnchor.constraint(equalToConstant: 150).isActive = true
                    catBtn.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
                    catBtn.layer.borderColor = .init(red: 1.0, green: 1.0, blue: 1.0, alpha: 1);
                    catBtn.addTarget(self, action: #selector(self.editCategoryClick), for: .touchUpInside)
                    self.categories_stackview.addArrangedSubview(catBtn)
                }
            }
        } else {
            for cat in categories_stackview.arrangedSubviews {
                let btn = cat as! UIButton
                if btn.titleLabel?.text == controller.oldValue {
                    if deleteCat == true {
                        categories_stackview.removeArrangedSubview(cat)
                        cat.removeFromSuperview()
                    } else {
                        btn.setTitle(controller.name_textfield.text, for: .normal)
                    }
                }
            }
            if deleteCat == true {
                categories = categories.filter{$0.name == controller.oldValue}
            } else {
                for var cat in categories {
                    if cat.name == controller.oldValue {
                        cat.name = controller.name_textfield.text ?? ""
                    }
                }
            }
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    @objc func editCategoryClick(_ sender: UIButton) {
        let button = sender
        let buttonFrame = button.frame
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "categoryView") as! AddCategoryViewController
        
        popoverContentController.modalPresentationStyle = .popover
        popoverContentController.delegate = self
        popoverContentController.new = false
        let name = button.titleLabel?.text ?? ""
        
        popoverContentController.oldValue = name
         
        if let popoverPresentationController = popoverContentController.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = buttonFrame
            popoverPresentationController.delegate = self
            present(popoverContentController, animated: true, completion: nil)
        }
    }
    
    @IBAction func addCategory(_ sender: UIButton) {
        let buttonFrame = sender.frame
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "categoryView") as! AddCategoryViewController
        
        popoverContentController.modalPresentationStyle = .popover
        popoverContentController.delegate = self
        popoverContentController.overview = true
         
        if let popoverPresentationController = popoverContentController.popoverPresentationController {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = buttonFrame
            popoverPresentationController.delegate = self
            present(popoverContentController, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        
        let im = closet.pieces[indexPath.row].image ?? ""
        let data = NSData(base64Encoded: im, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
        var image = UIImage(data: data as! Data)
        //cell.imageView.frame = CGRect(x: 0, y: 0, width: 100, height: 210)
        cell.imageView.image = image
        
        return cell;
    }
    
    
    let backgroundImageView = UIImageView()
    var user: User? = nil
    var closet: Closet = Closet()
    @IBOutlet weak var image_stackview: UIStackView!
    @IBOutlet weak var image_scrollview: UIScrollView!
    
    func didFinishCreate(controller: DetailViewController) {
        if let cloth = controller.item {
            closet.pieces.append(cloth)
            self.navigationController?.popViewController(animated: true)
        } else {
            controller.error_label.text = "Clothing item couldn't be inserted"
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Closet"
        setBackground()
        
        
        if let u = user {
            print(u.username + " just logged in!")
            loadClothesOfUserWith(id: u.id, token: u.token)
        }
        
        //doSyncTasks()
        //doAsyncTasks()
    }
    
    func loadClothesOfUserWith(id: Int, token: String) {
        print("Get Clothes of user")
        var request = URLRequest(url: URL(string: "http://localhost:3000/api/clothes")!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer " + token, forHTTPHeaderField: "authorization")

        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                if let msg = json["msg"] {
                    if msg as! String != "" {
                        print(msg)
                    }
                } else {
                    let jsonString = json["data"] as! Array<String>
                    for j in jsonString {
                       
                        if let jsonD = j.data(using: .utf8)
                        {
                            let decoder = JSONDecoder()

                            do {
                                let cloth = try decoder.decode(Clothing.self, from: jsonD)
                                self.closet.pieces.append(cloth)
                            } catch {
                                print(error.localizedDescription)
                            }
                        }
                    }
                    print("Clothes loaded successfully")
                }
                
            } catch {
                print("error")
            }
        })
        task.resume()
    }
    
    func doSyncTasks() {
        DispatchQueue.global().sync {
            print("SYNC 1 START")
            sleep(3)
            print("SYNC 1 SLEEPING")
            sleep(2)
            print("SYNC 1 ENDE")
        }
        DispatchQueue.global().sync {
            print("SYNC 2 START")
            sleep(2)
            print("SYNC 2 SLEEPING")
            sleep(1)
            print("SYNC 2 ENDE")
        }
        DispatchQueue.global().sync {
            print("SYNC 3 START")
            sleep(1)
            print("SYNC 3 SLEEPING")
            sleep(3)
            print("SYNC 3 ENDE")
        }
    }
    
    func doAsyncTasks() {
        DispatchQueue.global().async {
            print("TASK 1 START")
            sleep(3)
            print("TASK 1 SLEEPING")
            sleep(2)
            print("TASK 1 ENDE")
        }
        DispatchQueue.global().async {
            print("TASK 2 START")
            sleep(2)
            print("TASK 2 SLEEPING")
            sleep(1)
            print("TASK 2 ENDE")
        }
        DispatchQueue.global().async {
            print("TASK 3 START")
            sleep(1)
            print("TASK 3 SLEEPING")
            sleep(3)
            print("TASK 3 ENDE")
        }
    }
    @IBAction func createNewItem(_ sender: UIButton) {
        let createView = self.storyboard?.instantiateViewController(withIdentifier: "createClothing") as! DetailViewController
        let u = user
        createView.user = u
        createView.delegate = self
        self.navigationController?.pushViewController(createView, animated: true)
    }
    
    func setBackground() {
        view.addSubview(backgroundImageView)
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundImageView.image = UIImage(named: "wood.JPEG")
        view.sendSubviewToBack(backgroundImageView)
    }
}
